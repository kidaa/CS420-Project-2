import "gUnit" as gU
import "bstNode" as node

def test = object {
    class forMethod(p) {
        inherits gU.testCaseNamed(p)

        method testIsNullofNullNode {
            assert (node.null.isNull) shouldBe (true)
        }
        method testIsNullOfNonNullNode {
            assert (node.with("","").isNull) shouldBe (false)
        }
        method testAsStringOfNullNode {
            assert (node.null.asString) shouldBe ("null")
        }
        method testCopyOfNullNode {
            assert (node.null.copy.asString) shouldBe ("null")
        }
        method testCopyOfNonNullNode {
            def n = node.with(2,"b")
            def m = n.copy()
            assert (n.data) shouldBe (m.data)
        }
    }
}

print "bstNodes"
gU.testSuite.fromTestMethodsIn(test).runAndPrintResults
