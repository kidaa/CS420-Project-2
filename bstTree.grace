import "bstNode" as node

method with(*pairs) {
    withAll(list.withAll(pairs))
}

factory method withAll(pairs) {
    var root := node.null
    var size is readable := 0
    
    { addAll(pairs) }.apply

    method add(key, data) {
        addNode(node.with(key, data))
    }
    method addNode(newNode) {
        
        def addNode' = { newNode', currentNode -> 
            if (newNode'.isNull) then {
                currentNode
            } elseif (currentNode.isNull) then {
                size := size + 1
                newNode'
            } elseif (newNode'.key < currentNode.key) then {
                currentNode.left := addNode'.apply(newNode', currentNode.left)
                currentNode
            } elseif (newNode'.key > currentNode.key) then {
                currentNode.right := addNode'.apply(newNode', currentNode.right)
                currentNode
            } else {
                currentNode.data := newNode'.data
                currentNode
            }
        }

        root := addNode'.apply(newNode, root)
    }
    method addAll(newPairs) {
        newPairs.do { each -> add(each.key, each.value) }
    }
    method remove(key) {
        var removedNode := node.null
        var rightBranch := node.null

        def remove' = { key', parentNode, currentNode -> 
            if (currentNode.isNull) then {
                Exhausted.raise "Key not found"
            } elseif (key' < currentNode.key) then {
                currentNode.left := remove'.apply(key', currentNode, currentNode.left)
                currentNode
            } elseif (key' > currentNode.key) then {
                currentNode.right := remove'.apply(key', currentNode, currentNode.right)
                currentNode
            } else {
                size := size - 1
                removedNode := currentNode
                rightBranch := currentNode.right
                currentNode.left
            }
        }

        root := remove'.apply(key, node.null, root)
        addNode(rightBranch)
        removedNode.key::removedNode.data
    }
    method pop {
        if (isEmpty) then {
            Exhausted.raise "Tree is Empty"
        } else {
            remove(root.key)
        }
    }
    method at(key) {
        def at' = { key', currentNode -> 
            if (currentNode.isNull) then {
                Exhausted.raise "Key not found"
            } elseif (key' < currentNode.key) then {
                at'.apply(key', currentNode.left)
            } elseif (key' > currentNode.key) then {
                at'.apply(key', currentNode.right)
            } else {
                return currentNode.data
            }
        }
        at'.apply(key, root)
    }
    method isEmpty { root.isNull }
    method asString { root.asString }
    method copy {
        def newTree = with()
        newTree.addNode(root.copy) 
        newTree
    }
    method do(block1) { iterator.do(block1) }
    factory method iterator<T> {
        inherits iterable.trait<T>
        def tree = outer.copy
        method hasNext { !tree.isEmpty }
        method next {
            if (!hasNext) then { 
                Exhausted.raise "on Tree" 
            } else {
                tree.pop
            }
        }
    }
}
