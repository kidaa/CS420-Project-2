import "gUnit" as gU
import "bstTree" as tree

def test = object {
    class forMethod(m) {
        inherits gU.testCaseNamed(m)

        method testIsEmptyOfEmptyTree {
            assert (tree.with().isEmpty) shouldBe (true)
        }
        method testIssizeEmptyTree {
            assert (tree.with().size) shouldBe (0)
        }
        method testIsEmptyOfNonEmptyTree {
            assert (tree.with("a"::1).isEmpty) shouldBe (false)
        }
        method testSizeOfNonEmptyTree {
            assert (tree.with("a"::1).size) shouldBe (1)
        }
        method testSizeAfterRemoval {
            def t = tree.with(1::"a")
            assert (t.remove(1)) shouldBe (1::"a")
            assert (t.size) shouldBe (0)
        }
        method testAddToEmptyTree {
            def t = tree.with()
            t.add(1, 1)
            assert (t.isEmpty) shouldBe (false)
        }
        method testAtWithOneNode {
            def t = tree.with("a"::1)
            assert (t.at("a")) shouldBe (1)
        }
        method testAtWithALeftNode {
            def t = tree.with("b"::1, "a"::2)
            assert (t.at("a")) shouldBe (2)
            assert (t.at("b")) shouldBe (1)
        }
        method testAtWithARightNode {
            def t = tree.with("a"::1, "c"::2)
            assert (t.at("a")) shouldBe (1)
            assert (t.at("c")) shouldBe (2)
        }
        method testRemoveFromEmtpyTree {
            def t = tree.with()
            //assert (t.remove(1)) shouldRaise (Exhausted)
        }
        method testRemoveNodeWithNoChildrenFromTree {
            def t = tree.with(1::2)
            t.remove(1)
            assert (t.isEmpty) shouldBe (true)
        }
        method testRemoveNodeWithRightChildFromTree {
            def t = tree.with(1::"a", 2::"b")
            t.remove(1)
            assert (t.isEmpty) shouldBe (false)
            assert (t.at(2)) shouldBe ("b")
        }
        method testRemoveNodeWithLeftChildFromTree {
            def t = tree.with(2::"b", 1::"a")
            t.remove(2)
            assert (t.isEmpty) shouldBe (false)
            assert (t.at(1)) shouldBe ("a")
        }
        method testRemoveNodeWithBothChildrenFromTree {
            def t = tree.with(2::"b", 1::"a", 3::"c")
            t.remove(2)
            assert (t.isEmpty) shouldBe (false)
            assert (t.at(1)) shouldBe ("a")
            assert (t.at(3)) shouldBe ("c")
        }
        method testRemoveInnerNodeFromTree {
            def t = tree.with(4::"d", 2::"b", 1::"a", 3::"c")
            t.remove(2)
            assert (t.isEmpty) shouldBe (false)
            assert (t.at(1)) shouldBe ("a")
            assert (t.at(3)) shouldBe ("c")
            assert (t.at(4)) shouldBe ("d")
        }
        method testPop {
            def t = tree.with(1::"a", 2::"b")
            assert (t.pop) shouldBe (1::"a")
            assert (t.pop) shouldBe (2::"b")
            assert (t.isEmpty) shouldBe (true)
        }
        method testIterator {
            def t = tree.with(4::"d", 2::"b", 1::"a", 3::"c")
            def r = list.empty
            for (t) do { each -> r.addLast(each) }
            assert (r.contains(1::"a"))
            assert (r.contains(2::"b"))
            assert (r.contains(3::"c"))
            assert (r.contains(4::"d"))
        }
    }
}

print "bstTrees"
gU.testSuite.fromTestMethodsIn(test).runAndPrintResults
