CC = minigrace

ALL: bstNode bstTree
test: ALL bstNodeTest bstTreeTest

bstNode: bstNode.grace 
	$(CC) $<

bstNodeTest: bstNodeTest.grace 
	$(CC) $<

bstTree: bstTree.grace 
	$(CC) $<

bstTreeTest: bstTreeTest.grace 
	$(CC) $<

clean: 
	rm -f *.c *.gcn *.gct bstNode bstNodeTest bstTree bstTreeTest
