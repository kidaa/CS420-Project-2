method with(key, data) {
   withChildren(key, data, null, null) 
}

factory method withChildren(key', data', left', right') {
    var key is readable := key'
    var data is public := data'
    var left is public := left'
    var right is public := right'
    method isNull { false }
    method asString { "Node({key}, {data}, {left}, {right})" }
    method copy { 
        withChildren(key, data, left.copy, right.copy)
    }
}

def null = object {
    method isNull { true }
    method asString { "null" }
    method copy { null }
}
