import "bstTree" as bst

def defaultDataStructure = bst

method with(*pairs) {
    withAll (list.withAll(pairs)) withDataStructure (defaultDataStructure)
}

method withAll(pairs) {
    withAll (pairs) withDataStructure (defaultDataStructure)
}

factory method withAll(pairs) withDataStructure (dataStructure) {
    def data = dataStructure.withAll(pairs) 
    def size = { data.size }
    method isEmpty { data.isEmpty }
    method containsKey(key) {
        try {
            data.at(key)
            true
        } catch { exception:Exhausted -> return false } 
            false
    }
    method containsValue(value) { values.asList.contains(value) }
    method at(key) ifAbsent(action) {
        if (containsKey(key)) then {
            data.at(key)
            action.apply
        }
    }
    method at(key)put(value) {
        data.add(key,value)
        self
    }
    method []:=(key, value) {
        at(key) put(value)
    }
    method at(key) { data.at(key) }
    method [](key) { at(key) }
    method removeAllKeys(keys) { keys.do { each -> removeKey(each) } }
    method removeKey(key) { data.remove(key) }
    //removeAllValues(removals:Collection<T>) -> Dictionary<K,T>
    //removeValue(*removals:T) -> Dictionary<K,T>
    factory method keys {
        inherits iterable.trait
        def dataIterator = data.iterator
        method hasNext { !dataIterator.hasNext }
        method next {
            if (!hasNext) then { 
                Exhausted.raise "on Dictionary" 
            } else {
                data.next.key
            }
        }
    }
    factory method values {
        inherits iterable.trait
        def dataIterator = data.iterator
        method hasNext { !dataIterator.hasNext }
        method next {
            if (!hasNext) then { 
                Exhausted.raise "on Dictionary" 
            } else {
                data.next.key
            }
        }
    }
    //bindings -> Iterator<Binding<K,T>>
    //keysAndValuesDo(action:Block2<K,T,Done>) -> Done
    method keysDo(action) { keys.do { action } }
    method valuesDo(action) { values.do { action } }
    //do(action:Block1<T,Done>) -> Done
    method ==(other) {
        if (size != other.size) then { return false }
        def selfIterator = self.iterator
        def otherIterator = other.iterator
        while {selfIterator.hasNext && otherIterator.hasNext} do {
            if (selfIterator.next != otherIterator.next) then {
                return false
            }
        }
        true
    }
    //copy -> Dictionary<K,T>
}
